FROM openjdk:8-jre-alpine
RUN mkdir -p /opt/app/resources
RUN mkdir -p /opt/app/job-queues
WORKDIR /opt/app
COPY ./src/scripts/run_jar.sh ./target/uberjar/job-queues-0.1.0-SNAPSHOT-standalone.jar ./
COPY ./resources/sample-input.json ./resources/
RUN ["chmod", "+x", "./run_jar.sh"]
ENTRYPOINT ["./run_jar.sh"]
