(ns com.ricardomiranda.resources.job-json)

(def woker-json-BoJack
    "{
        \"id\": \"8ab86c18-3fae-4804-bfd9-c3d6e8f66260\",
        \"name\": \"BoJack Horseman\",
        \"primary_skillset\": [\"bills-questions\"],
        \"secondary_skillset\": []
     }")

(def job-01
    "{
        \"id\": \"f26e890b-df8e-422e-a39c-7762aa0bac36\",
        \"type\": \"rewards-question\",
        \"urgent\": false
    }")

(def job-02
    "{
        \"id\": \"c0033410-981c-428a-954a-35dec05ef1d2\",
        \"type\": \"bills-questions\",
        \"urgent\": true
    }")

(def job-request-01
    "{
        \"agent_id\": \"8ab86c18-3fae-4804-bfd9-c3d6e8f66260\"
      }")

(def job-request-02
    "{
        \"agent_id\": \"ed0e23ef-6c2b-430c-9b90-cd4f1ff74c88\"
      }")

(defn worker1 [worker-id worker-name primary-skills secondary-skills]
    {:worker-id worker-id
     :worker-name worker-name
     :primary-skills primary-skills
     :secondary-skills secondary-skills})

(def user-01 (worker1 "8ab86c18-3fae-4804-bfd9-c3d6e8f66260" "BoJack Horseman" #{"bills-questions"} #{}))

(def user-02 
    (worker1 
        "ed0e23ef-6c2b-430c-9b90-cd4f1ff74c88" 
        "Mr. Peanut Butter" 
        #{"rewards-question"} 
        #{"bills-questions"}))
      
(def workers-test [user-01 user-02])
