(ns com.ricardomiranda.job-queues.queues-test
    (:require 
        [clojure.test :refer :all]
        [com.ricardomiranda.job-queues.queues :refer :all]
        [com.ricardomiranda.resources.topic-queues :refer :all]
        [com.ricardomiranda.resources.topics :refer :all]))

(deftest queues-empty-test-1
    (testing "Is a topic-queue map with empty queues created from scratch"
        (let [q (topic-queue :do_it clojure.lang.PersistentQueue/EMPTY clojure.lang.PersistentQueue/EMPTY)]
            (is (and 
                (= (get q :high-priority) clojure.lang.PersistentQueue/EMPTY) 
                (= (get q :high-priority) clojure.lang.PersistentQueue/EMPTY))))))
             
(deftest queues-empty-test-2
    (testing "Is a topic-queue map with empty queues created from resource"
        (let [q empty-topic]
            (is (and 
                (= (get q :high-priority) clojure.lang.PersistentQueue/EMPTY) 
                (= (get q :high-priority) clojure.lang.PersistentQueue/EMPTY))))))

(deftest topic-h-empty-topic-test
    (testing "peek no list with high priority job from empty queue"
        (let [expected nil
              actual (topic-high-priority {"do_something" empty-topic})]
            (is (= actual expected)))))

(deftest topic-h-empty--test
    (testing "peek no list with high priority job from empty map"
        (let [expected nil
              actual (topic-high-priority {})]
            (is (= actual expected)))))

(deftest topic-h-one-high-test
    (testing "Peek first list with high priority job from queue"
        (let [expected one-high-topic
              actual (topic-high-priority {"do_something" one-high-topic})]
            (is (= actual expected)))))

(deftest peek-l-one-high-test
    (testing "Peek first list with low priority job from queue"
        (let [expected one-low-topic
              actual (topic-low-priority {"do_something" one-low-topic})]
            (is (= actual expected)))))

(deftest peek-pop-high-queue-empty-test
    (testing "Peek and pop high priority job from empty queue"
        (let [expected {:job nil :topic-queue empty-topic}
              actual (peek-pop-high-queue empty-topic)]
            (is (= actual expected)))))

(deftest peek-pop-high-queue-test
    (testing "Peek and pop high priority job from queue"
        (let [expected {:job "f-h" :topic-queue empty-topic}
              actual (peek-pop-high-queue one-high-topic)]
            (is (= actual expected)))))

(deftest peek-pop-low-queue-test
    (testing "Peek and pop high priority job from queue"
        (let [expected {:job "f-l" :topic-queue empty-topic}
              actual (peek-pop-low-queue one-low-topic)]
            (is (= actual expected)))))
            
(deftest select-job-from-topics-empty-topic-test
    (testing "Select a job form an ampty topic"
        (let [expected {:job nil :topic-queue nil}
              actual (select-job-from-topics {"do_something" empty-topic})]
            (is (= actual expected)))))

(deftest select-job-from-topics-empty-map-test
    (testing "Select a job form an ampty map"
        (let [expected {:job nil :topic-queue nil}
              actual (select-job-from-topics {})]
            (is (= actual expected)))))

(deftest select-job-from-do-pink-test
    (testing "Select a job form do-pink"
        (let [expected {:job "f-h-p" :topic-queue (assoc do-pink :high-priority (pop (get do-pink :high-priority)))}
              actual (select-job-from-topics {"do_pink" do-pink})]
            (is (= actual expected)))))

(deftest worker-test
    (testing "Creates an worker"
        (let [expected 
                {:worker-id :Anna
                 :worker-name :Anna
                 :primary-skills #{"do_something"}
                 :secondary-skills #{:do_it}}
              actual (worker :Anna :Anna #{"do_something"} #{:do_it})]
            (is (= actual expected)))))

(deftest primary-skills-topics-worker-can-listen-from-empty-map-test
    (testing "Find topics for primary skill of a worker from empty map"
        (let [expected {}
              actual (primary-skills-topics-worker-can-listen Mac {})]
            (is (= actual expected)))))

(deftest primary-skills-topics-worker-can-listen-Hugo-test
    (testing "Find topics for primary skill of worker Hogo from topics"
        (let [expected {"do_pink" do-pink}
              actual (primary-skills-topics-worker-can-listen Hugo topics-test)]
            (is (= actual expected)))))

(deftest secondary-skills-topics-worker-can-listen-Hugo-test
    (testing "Find topics for secondary skill of worker Hogo from topics"
        (let [expected {}
              actual (secondary-skills-topics-worker-can-listen Hugo topics-test)]
            (is (= actual expected)))))

(deftest topics-worker-can-listen-Hugo-test
    (testing "Find topics for worker Hugo from topics"
        (let [expected 
                {:worker-id "9b90-cd4f1ff74c88"
                 :worker-name "Hugo"
                 :primary-skills {"do_pink" do-pink}
                 :secondary-skills {}}
              actual (topics-worker-can-listen Hugo topics-test)]
            (is (= actual expected)))))

(deftest topics-worker-can-listen-Mac-test
    (testing "Find topics for worker Mac from topics"
        (let [expected 
                {:worker-id "4804-bfd9-c3d6e8f66260" 
                 :worker-name "Mac" 
                 :primary-skills {"do_red" do-red, "do_green" do-green}
                 :secondary-skills {"do_yellow" do-yellow}}
              actual (topics-worker-can-listen Mac topics-test)]
            (is (= actual expected)))))

(deftest select-job-Mac-empty-list-test
    (testing "Selecting job from empty list returns nil"
        (let [expected 
                {:worker-id "4804-bfd9-c3d6e8f66260" 
                 :worker-name "Mac" 
                 :job nil 
                 :topic-queue nil}
              actual (select-job Mac {})]
             (is (= actual expected)))))

(deftest select-job-Hugo-from-topics-test
    (testing "Selecting job for Hugo from topics"
        (let [expected    
                {:worker-id "9b90-cd4f1ff74c88"
                 :worker-name "Hugo"
                 :job "f-h-p"
                 :topic-queue (assoc do-pink :high-priority (pop (get do-pink :high-priority)))}
              actual (select-job Hugo topics-test)]
            (is (= actual expected)))))

(deftest select-job-Mac-from-topics-test
    (testing "Selecting job for Mac from topics"
        (let [expected    
                {:worker-id "4804-bfd9-c3d6e8f66260"
                 :worker-name "Mac"
                 :job "f-h-r"
                 :topic-queue (assoc do-red :high-priority (pop (get do-red :high-priority)))}
              actual (select-job Mac topics-test)]
            (is (= actual expected)))))

(deftest job-test
    (testing "Defining a job"
        (let [expected 
                {:job-id "f26e890b-df8e-422e-a39c-7762aa0bac36"
                 :queue-name "rewards-question"
                 :priority :low-priority}
              actual (job "f26e890b-df8e-422e-a39c-7762aa0bac36" "rewards-question" :low-priority)]
            (is (= actual expected)))))

(deftest new-job-02-test
   (testing "Add job to queue 02"
       (let [j (job "f26e890b-df8e-422e-a39c-7762aa0bac36" "rewards-question" :high-priority)
             nt (add-job-topic j topics)
             q (get nt "rewards-question")
             expected j
             actual (peek (get q :high-priority))]
           (is (= actual expected)))))

(deftest new-job-03-test
   (testing "Add job to queue 03"
       (let [j (job "f26e890b-df8e-422e-a39c-7762aa0bac36" "rewards-question" :low-priority)
             nt (add-job-topic j topics)
             q (get nt "rewards-question")
             expected j
             actual (peek (get q :low-priority))]
           (is (= actual expected)))))

(deftest select-job-with-worker-id-empty-workers-empty-topics-test
   (testing "Select job using worker id with empty workers list and empty topics list"
       (let [expected {:job nil :topic-queue nil :worker-name nil :worker-id nil}
             actual (select-job-with-worker-id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260" [] {})]
            (is (= actual expected)))))

(deftest select-job-with-worker-id-no-jobs-test
   (testing "Select job using worker id with no jobs on queue"
       (let [expected
               {:job nil
                :topic-queue nil
                :worker-name "BoJack Horseman"
                :worker-id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260"}
             actual (select-job-with-worker-id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260" workers-test topics)]
           (is (= actual expected)))))

(deftest select-job-with-worker-id-no-job-in-listening-queue-test
    (testing "Select job using worker id with no jobs in queue he is listening"
        (add-job-topic (job "f26e890b-df8e-422e-a39c-7762aa0bac36" "rewards-question" :low-priority) topics)
        (let [expected 
                {:job nil 
                 :topic-queue nil
                 :worker-name "BoJack Horseman"
                 :worker-id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260"}
              actual (select-job-with-worker-id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260" workers-test topics)]
            (is (= actual expected)))))

(deftest select-job-with-worker-id-test
    (testing "Select job using worker id"
        (let [j (job "f26e890b-df8e-422e-a39c-7762aa0bac36" "rewards-question" :high-priority)
              nt (add-job-topic j topics)
              expected 
                {:job j
                 :topic-queue rewards-question
                 :worker-name "Mr. Peanut Butter"
                 :worker-id "ed0e23ef-6c2b-430c-9b90-cd4f1ff74c88"}
              actual 
                (select-job-with-worker-id 
                    "ed0e23ef-6c2b-430c-9b90-cd4f1ff74c88" 
                    workers-test 
                    nt)]
                (is (= actual expected)))))

(deftest select-job-with-worker-id-Mac-test
    (testing "Select job using worker id for Mac"
        (let [expected 
                {:job "f-h-r"
                 :topic-queue (assoc do-red :high-priority (pop (get do-red :high-priority)))
                 :worker-name "Mac"
                 :worker-id "4804-bfd9-c3d6e8f66260"}
              actual 
                (select-job-with-worker-id
                    "4804-bfd9-c3d6e8f66260"
                    workers-02-test 
                    topics-test)]
                (is (= actual expected)))))


