(ns com.ricardomiranda.job-queues.queues)

;; Queues of jobs

(defn topic-queue [queue-name high-priority low-priority] 
    {:queue-name queue-name, :high-priority high-priority, :low-priority low-priority})
  
(defn topic-by-priority [priority topics]
    (second (first (filter (fn [x] (peek (get (second x) priority))) topics))))

(defn topic-high-priority [topics]
    (topic-by-priority :high-priority topics))
  
(defn topic-low-priority [topics]
    (topic-by-priority :low-priority topics))

(defn peek-pop-queue [priority topic-queue]
    {:job (peek (get topic-queue priority)), :topic-queue (assoc topic-queue priority (pop (get topic-queue priority)))})

(defn peek-pop-high-queue [topic-queue]
    (peek-pop-queue :high-priority topic-queue))

(defn peek-pop-low-queue [topic-queue]
    (peek-pop-queue :low-priority topic-queue))

(defn select-job-from-topics [topics]
    (let [topic-high (topic-high-priority topics)
          topic-low (topic-low-priority topics)]
        (cond
            topic-high (peek-pop-high-queue topic-high)
            topic-low (peek-pop-low-queue topic-low)
            :else {:job nil :topic-queue nil})))

;; Jobs

(defn job [job-id queue-name priority]
    {:job-id job-id, :queue-name queue-name, :priority priority})

(defn add-job-topic [job topics]
    (let [t (get job :queue-name)
          p (get job :priority)]
        (zipmap 
            (keys topics) 
            (map (fn [x] (if (= (get x :queue-name) t) (assoc x p (conj (get x p) job)) x)) (vals topics)))))

;; Agents (workers)

(defn worker [worker-id worker-name primary-skills secondary-skills]
    {:worker-id worker-id, :worker-name worker-name, :primary-skills primary-skills, :secondary-skills secondary-skills})

(defn select-topics-worker-can-listen [skills worker topics]
    (let [worker-skills (get worker skills)]
     (select-keys topics (for [[k v] topics :when (contains? worker-skills (get v :queue-name))] k))))

(defn primary-skills-topics-worker-can-listen [worker topics]
    (select-topics-worker-can-listen :primary-skills worker topics))

(defn secondary-skills-topics-worker-can-listen [worker topics]
    (select-topics-worker-can-listen :secondary-skills worker topics))

;; Linking Jobs and workers

 (defn topics-worker-can-listen [worker topics]
     {:worker-id (get worker :worker-id)
      :worker-name (get worker :worker-name)
      :primary-skills (primary-skills-topics-worker-can-listen worker topics)
      :secondary-skills (secondary-skills-topics-worker-can-listen worker topics)})

(defn select-job [worker topics]
    (let [topics-worker (topics-worker-can-listen worker topics)
          primary-skills (get topics-worker :primary-skills)
          secondary-skills (get topics-worker :secondary-skills)
          job-selected-primary (select-job-from-topics primary-skills)
          job-selected-secondary (select-job-from-topics secondary-skills)
          job
            (cond
                (get job-selected-primary :job) job-selected-primary 
                (get job-selected-secondary :job) job-selected-secondary
                :else {:job nil :topic-queue nil})]
        (assoc job :worker-name (get worker :worker-name) :worker-id (get worker :worker-id))))

(defn select-job-with-worker-id [worker-id workers topics]
    (let [w (first (filter (fn [x] (= worker-id (get x :worker-id))) (vals workers)))]
        (select-job w topics)))
