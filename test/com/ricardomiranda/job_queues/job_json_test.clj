(ns com.ricardomiranda.job-queues.job-json-test
    (:require 
        [clojure.data.json :as json]
        [clojure.test :refer :all]
        [com.ricardomiranda.job-queues.queues :refer :all]
        [com.ricardomiranda.job-queues.job-json :refer :all]
        [com.ricardomiranda.resources.job-json :refer :all]
        [com.ricardomiranda.resources.topics :refer :all]))

(deftest  new-agent-BoJack-test   
    (testing "A new worker is created form a json string"
        (let [expected
                {:worker-id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260"
                 :worker-name "BoJack Horseman"
                 :primary-skills #{"bills-questions"}
                 :secondary-skills #{}}
              actual (new-agent (json/read-str woker-json-BoJack))]
            (is (= actual expected)))))

(deftest add-agent-BoJack-to-empty-map-test
    (testing "A new worker is joined to an empty map of workers workers"
        (let [expected 
                {"8ab86c18-3fae-4804-bfd9-c3d6e8f66260"
                    {:worker-id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260"
                    :worker-name "BoJack Horseman"
                    :primary-skills #{"bills-questions"}
                    :secondary-skills #{}}
                }
              actual (add-agent {} (json/read-str woker-json-BoJack))]
            (is (= actual expected)))))
        
(deftest add-agent-BoJack-test
    (testing "A new worker is joined to a map of workers"
        (let [expected 
                {"8ab86c18-3fae-4804-bfd9-c3d6e8f66260"
                    {:worker-id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260"
                    :worker-name "BoJack Horseman"
                    :primary-skills #{"bills-questions"}
                    :secondary-skills #{}}
                }
              actual 
                (add-agent 
                    {"8ab86c18-3fae-4804-bfd9-c3d6e8f66260"
                        {:worker-id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260"
                         :worker-name "BoJack Horseman"
                         :primary-skills #{"bills-questions"}
                         :secondary-skills #{}}
                    } 
                (json/read-str woker-json-BoJack))]
            (is (= actual expected)))))
        
(deftest new-job-01-test
    (testing "A new job from a json string"
        (let [expected 
                {:job-id "f26e890b-df8e-422e-a39c-7762aa0bac36"
                 :queue-name "rewards-question"
                 :priority :low-priority}
              actual (new-job (json/read-str job-01))] 
            (is (= actual expected)))))

(deftest new-job-02-test
    (testing "A very new job from a json string"
        (let [expected 
                {:job-id "c0033410-981c-428a-954a-35dec05ef1d2"
                 :queue-name "bills-questions"
                 :priority :high-priority}
              actual (new-job (json/read-str job-02))] 
            (is (= actual expected)))))

(deftest add-job-01-test
    (testing "Add a job to its queue"
        (let [expected 
                {"bills-questions" bills-questions
                 "rewards-question" (assoc
                                        rewards-question 
                                        :low-priority 
                                        (conj (get rewards-question :low-priority) 
                                            {:job-id "f26e890b-df8e-422e-a39c-7762aa0bac36"
                                             :queue-name "rewards-question"
                                             :priority :low-priority}))
                 "hard-questions" hard-questions}
              actual (add-job topics (json/read-str job-01))]
            (is (= actual expected)))))

(deftest add-job-02-test
    (testing "Add another job to its queue"
        (let [expected 
                {"bills-questions" (assoc
                                        bills-questions
                                        :high-priority
                                        (conj (get bills-questions :low-priority) 
                                            {:job-id "c0033410-981c-428a-954a-35dec05ef1d2"
                                             :queue-name "bills-questions"
                                             :priority :high-priority}))
                 "rewards-question" rewards-question 
                 "hard-questions" hard-questions}
              actual (add-job topics (json/read-str job-02))]
            (is (= actual expected)))))
        
(deftest new-job-request-01-test
    (testing "Create job request 01"
        (let [expected "8ab86c18-3fae-4804-bfd9-c3d6e8f66260"
              actual (new-job-request (json/read-str job-request-01))]
            (is (= actual expected)))))

(deftest new-job-request-02-test
    (testing "Create job request 02"
        (let [expected "ed0e23ef-6c2b-430c-9b90-cd4f1ff74c88"
              actual (new-job-request (json/read-str job-request-02))]
            (is (= actual expected)))))

(deftest create-report-job-assigned-test
    (testing "Create a job report" 
        (let [expected "{\"job_assigned\":{\"job_id\":\"c0033410-981c-428a-954a-35dec05ef1d2\",\"agent_id\":\"8ab86c18-3fae-4804-bfd9-c3d6e8f66260\"}}"
              actual 
                (create-report-job-assigned 
                    "c0033410-981c-428a-954a-35dec05ef1d2" 
                    "8ab86c18-3fae-4804-bfd9-c3d6e8f66260")]
            (is (= actual expected)))))

(deftest process-intput-file-test
    (testing "Dispatching"
        (let [expected 
                [
                    {
                        "job_assigned" {
                            "job_id" "c0033410-981c-428a-954a-35dec05ef1d2",
                            "agent_id" "8ab86c18-3fae-4804-bfd9-c3d6e8f66260",
                        },
                    },
                    {
                        "job_assigned" {
                            "job_id" "f26e890b-df8e-422e-a39c-7762aa0bac36",
                            "agent_id" "ed0e23ef-6c2b-430c-9b90-cd4f1ff74c88",
                        }
                    }
                ]
              actual (process-intput-file nil nil)]
            (is (= actual expected)))))
