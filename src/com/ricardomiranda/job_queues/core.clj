(ns com.ricardomiranda.job-queues.core
    (:gen-class)
    (:require 
        [clojure.java.io :as io :refer :all]
        [clojure.string :as str :refer :all]
        [com.ricardomiranda.job-queues.job-json :refer :all]
        [com.ricardomiranda.resources.topics :refer :all]))

(defn ask-user [continue?]
    (if (= continue? "y")
        ((println "What is the name of the file to be processed (.json)?")
         (let [file-with-jobs (trim (read-line))]
            ((println "What is the name of the output file?")
            (let [output-file (trim (read-line))]
                (process-intput-file file-with-jobs output-file)
                (println "Do you want to process another file (y / n)?")
                (let [reply (trim (read-line))]
                    (ask-user reply))
                    ((println "Exiting program") 
                     (System/exit 0))))))))

(defn -main
    "Simplified version of a job queue."
    [& args]
    (println "Start processing jobs")
    (ask-user "y"))

