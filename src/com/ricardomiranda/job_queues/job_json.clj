(ns com.ricardomiranda.job-queues.job-json
    (:require
        [clojure.java.io :as io :refer :all]
        [clojure.data.json :as json]
        [com.ricardomiranda.job-queues.queues :refer :all]
        [com.ricardomiranda.resources.topics :refer :all]))

(def state {:workers {} :topics topics :output []})

;; Add a new agent (worker)

(defn new-agent [worker-json]
        (worker 
            (get worker-json "id")
            (get worker-json "name")
            (into #{} (get worker-json "primary_skillset"))
            (into #{} (get worker-json "secondary_skillset"))))

(defn add-agent [current-workers worker-json]
    (let [w (new-agent worker-json)]
        (assoc current-workers (get w :worker-id) w)))

;; Add a new job

(defn new-job [job-json]
    (let [priority (if (get job-json "urgent") :high-priority :low-priority)]
        (job
            (get job-json "id")
            (get job-json "type")
            priority)))

(defn add-job [current-topics job-json]
    (let [j (new-job job-json)]
        (add-job-topic j current-topics)))

;; Process a job request for an agent

(defn new-job-request [job-request-json]
    (let [job-request job-request-json]
        (get job-request "agent_id")))

(defn job-request [job-request-json current-workers current-topics]
    (let [worker-id (new-job-request job-request-json)]
        (select-job-with-worker-id worker-id current-workers current-topics)))

;; Program output
   
(defn create-report-job-assigned [job-id agent-id]
    (let [r {"job_assigned" {"job_id" job-id "agent_id" agent-id}}]
        (json/write-str r)))

(defn create-output-file [write-file output]
    (spit write-file (json/write-str output)))

;; Program input

(defn read-file [input-file]
    (let [default-file "./resources/sample-input.json"]
    (with-open [reader (io/reader (if input-file input-file default-file))]
        (json/read-str (slurp reader)))))

;; Dispatch requests to its corresponding processor

(defn dispatch [x current-state]
    (let [a (get x "new_agent")
          j (get x "new_job")
          r (get x "job_request")]
        (cond 
            a (assoc current-state :workers (add-agent (get current-state :workers) a))
            j (assoc current-state :topics (add-job (get current-state :topics) j))
            r (let [x (job-request r (get current-state :workers) (get current-state :topics))]
                (assoc current-state :output (conj (get current-state :output) x)))
            :else current-state)))

;; Process input file

(defn loop-requests [current-state xs]
    (let [x (first xs)
          tail (rest xs)
          new-state (dispatch x current-state)]
          (if (= (count tail) 0) new-state (loop-requests new-state tail))))

(defn process-intput-file [input-file write-file]
        (let [current-state (loop-requests state (read-file input-file))
              xs (map 
                    (fn [x] {"job_assigned" {"job_id" (get (get x :job) :job-id), "agent_id" (get x :worker-id)}}) 
                    (get current-state :output))]
            (if write-file (create-output-file write-file xs) xs)))

