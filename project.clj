(defproject job-queues "0.1.0-SNAPSHOT"
  :description "Simplified version of a job queue, written in Clojure."
  :url "https://bitbucket.org/miranda_ricardo/clojure-job-queue"
  :license {:name "MIT"
            :url "queue://opensource.org/licenses/MIT"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/data.codec "0.1.1"]
                 [org.clojure/data.json "0.2.6"]
                 [cheshire "5.8.0"]
                 [clj-time "0.14.4"]]
  :main ^:skip-aot com.ricardomiranda.job-queues.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
