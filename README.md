# clojure-job-queues

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

Simplified version of a job queue, written in Clojure.

## Run program
Clone project to your machine.
Go to project folder.
Run instructions:

```bash
lein uberjar
docker build -f ./Dockerfile . -t queues
docker run -v ~/job-queues:/opt/app/job-queues -it queues
```

You will be asked what is the input file.
You will be asked what is the output file.
Put all files in the docker shared folder:


```bash
./job-queues/
```

For instance copy input file to:

```bash
~/job-queues/sample-input.json
```

And interact with program like so:

```bash
What is the name of the file to be processed (.json)?
./job-queues/sample-input.json
What is the name of the output file?
./job-queues/out.json
```

## Test program
To test the program run:

```bash
lein test
```

## Requisites
You must have docker in your machine.

## Explanation
Job queues were created in record:

```clojure
    {:queue-name queue-name, :high-priority high-priority, :low-priority low-priority}
```

In fact 2 queues were are created for each type of messages, a high priority and a low priority. Each new message will be placed in a priority according to its urgency.

Workers are created with record:

```clojure
    {:worker-id worker-id, :worker-name worker-name, :primary-skills primary-skills, :secondary-skills secondary-skills}
```

Each worker has a set of primary skills and a set of secondary skills.

When a worker requests a job, first an available job is searched in its primary skills, with preference for a high priority one.
If no job were assign then an available secondary skill is searched, giving preference for high priority jobs.

Same memoization techniques could be applied but for this demo it would be overkill. I agree that "premature optimization is the root of all evil" [David Knuth](https://en.wikipedia.org/wiki/Donald_Knuth "David Knuth"). I chose what seemed the most efficient data types according to usage. I did not do any profiling to optimize code.

In a production system I would create a log but in this exercise I did not.

## Authors
*   [Ricardo Miranda](https://github.com/ricardomiranda)

## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
