(ns com.ricardomiranda.resources.topic-queues
    (:require 
        [com.ricardomiranda.job-queues.queues :refer :all]))

(def empty-topic 
        (topic-queue "do_something" clojure.lang.PersistentQueue/EMPTY clojure.lang.PersistentQueue/EMPTY))

(def one-high-topic 
        (topic-queue 
                "do_something"
                (conj clojure.lang.PersistentQueue/EMPTY "f-h") 
                clojure.lang.PersistentQueue/EMPTY))

(def one-low-topic 
        (topic-queue 
                "do_something"
                clojure.lang.PersistentQueue/EMPTY 
                (conj clojure.lang.PersistentQueue/EMPTY "f-l")))

(def one-high-low-topic 
        (topic-queue 
                "do_something"
                (conj clojure.lang.PersistentQueue/EMPTY "f-h") 
                (conj clojure.lang.PersistentQueue/EMPTY "f-l")))

(def do-red
        (topic-queue 
                "do_red"
                (conj clojure.lang.PersistentQueue/EMPTY "f-h-r" "s-h-r" "t-h-r") 
                (conj clojure.lang.PersistentQueue/EMPTY "f-l-r")))

(def do-green
        (topic-queue
                "do_green"
                (conj clojure.lang.PersistentQueue/EMPTY "f-h-g") 
                (conj clojure.lang.PersistentQueue/EMPTY "f-l-g" "s-l-g" "t-l-g")))

(def do-yellow
        (topic-queue
                "do_yellow"
                (conj clojure.lang.PersistentQueue/EMPTY "f-h-y") 
                (conj clojure.lang.PersistentQueue/EMPTY "f-l-y" "s-l-y" "t-l-y")))

(def do-grey
        (topic-queue
                "do_grey"
                (conj clojure.lang.PersistentQueue/EMPTY "f-h-gr") 
                (conj clojure.lang.PersistentQueue/EMPTY "f-l-gr" "s-l-gr" "t-l-gr")))

(def do-pink 
        (topic-queue
                "do_pink"
                (conj clojure.lang.PersistentQueue/EMPTY "f-h-p") 
                clojure.lang.PersistentQueue/EMPTY))

(def topics-test 
        {(get do-red :queue-name) do-red
         (get do-green :queue-name) do-green
         (get do-yellow :queue-name) do-yellow
         (get do-grey :queue-name) do-grey 
         (get do-pink :queue-name) do-pink})

(def Mac (worker "4804-bfd9-c3d6e8f66260" "Mac" #{"do_red" "do_green"} #{"do_yellow"}))

(def Hugo (worker "9b90-cd4f1ff74c88" "Hugo" #{"do_pink"} #{}))

(def workers-02-test {"4804-bfd9-c3d6e8f66260" Mac,"9b90-cd4f1ff74c88" Hugo})

(def user-01 (worker "8ab86c18-3fae-4804-bfd9-c3d6e8f66260" "BoJack Horseman" #{"bills-questions"} #{}))

(def user-02 
    (worker 
        "ed0e23ef-6c2b-430c-9b90-cd4f1ff74c88" 
        "Mr. Peanut Butter" 
        #{"rewards-question"} 
        #{"bills-questions"}))

(def workers-test {"8ab86c18-3fae-4804-bfd9-c3d6e8f66260" user-01, "ed0e23ef-6c2b-430c-9b90-cd4f1ff74c88" user-02})


