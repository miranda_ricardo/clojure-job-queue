(ns com.ricardomiranda.resources.topics
    (:require 
        [com.ricardomiranda.job-queues.queues :refer :all]))

(def hard-questions
    (topic-queue "hard-questions" clojure.lang.PersistentQueue/EMPTY clojure.lang.PersistentQueue/EMPTY))

(def rewards-question
    (topic-queue "rewards-question" clojure.lang.PersistentQueue/EMPTY clojure.lang.PersistentQueue/EMPTY))

(def bills-questions
    (topic-queue "bills-questions" clojure.lang.PersistentQueue/EMPTY clojure.lang.PersistentQueue/EMPTY))

(def topics
    {(get bills-questions :queue-name) bills-questions
     (get rewards-question :queue-name) rewards-question
     (get hard-questions :queue-name) hard-questions})